from django.db import models


class Word(models.Model):
    word = models.CharField(max_length=16)


class Clue(models.Model):
    game_id = models.CharField(max_length=32)

    white_team = models.BooleanField()

    indication1 = models.IntegerField()
    indication2 = models.IntegerField()
    indication3 = models.IntegerField()

    clue1 = models.CharField(max_length=64)
    clue2 = models.CharField(max_length=64)
    clue3 = models.CharField(max_length=64)

    guess1 = models.IntegerField()
    guess2 = models.IntegerField()
    guess3 = models.IntegerField()

    foe_guess1 = models.IntegerField()
    foe_guess2 = models.IntegerField()
    foe_guess3 = models.IntegerField()


class GameWords(models.Model):
    game_id = models.CharField(max_length=32)

    white_word1 = models.CharField(max_length=16)
    white_word2 = models.CharField(max_length=16)
    white_word3 = models.CharField(max_length=16)
    white_word4 = models.CharField(max_length=16)

    black_word1 = models.CharField(max_length=16)
    black_word2 = models.CharField(max_length=16)
    black_word3 = models.CharField(max_length=16)
    black_word4 = models.CharField(max_length=16)


class Player(models.Model):
    game_id = models.CharField(max_length=32)
    name = models.CharField(max_length=16)
    white_team = models.BooleanField()
    is_spy = models.BooleanField(default=False)


class Game(models.Model):
    game_id = models.CharField(max_length=32)
    private = models.BooleanField()
    white_starting = models.BooleanField()

    white_errors = models.IntegerField(default=0)
    black_errors = models.IntegerField(default=0)
    white_success = models.IntegerField(default=0)
    black_success = models.IntegerField(default=0)

    state = models.IntegerField(default=0)
    turn = models.IntegerField(default=1)
