from rest_framework import serializers

from backend.models import Game, Player, GameWords


class GameCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('game_id', 'private')


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('game_id',
                  'private',
                  'white_starting',
                  'white_errors',
                  'black_errors',
                  'white_success',
                  'black_success',
                  'state',
                  'turns')


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ('game_id', 'name', 'white_team', 'is_spy')


class WhiteWordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameWords
        field = ('white_word1', 'white_word2', 'white_word3', 'white_word4')


class BlackWordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameWords
        field = ('black_word1', 'black_word2', 'black_word3', 'black_word4')
