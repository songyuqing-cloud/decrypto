import os
import redis

from singleton_decorator import singleton


@singleton
class Redis:
    def __init__(self):
        self.redis_instance = redis.StrictRedis(host=os.environ['REDIS_HOST'],
                                                port=os.environ['REDIS_PORT'], db=0)

    def set(self, key, value):
        return self.redis_instance.set(key, value)

    def get(self, key):
        return self.redis_instance.get(key)

    def delete(self, key):
        self.redis_instance.delete(key)
