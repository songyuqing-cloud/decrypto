import random

from backend.models import Word, GameWords, Game


def generate_game(game_id, private):
    Game.objects.create(game_id=game_id,
                        private=private,
                        white_starting=bool(random.randint(0, 1)))
    words = generate_words()
    GameWords.objects.create(game_id=game_id,
                             white_word1=words[0],
                             white_word2=words[1],
                             white_word3=words[2],
                             white_word4=words[3],
                             black_word1=words[4],
                             black_word2=words[5],
                             black_word3=words[6],
                             black_word4=words[7])


def generate_words():
    words = Word.objects.all().order_by('?')[:8]
    return list(map(lambda w: w.word, words))
