import json

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.utils.enums import RequestTypeEnum
from backend.utils.game_creation import generate_game
from backend.utils.game_manager import get_game, is_private, delete_game
from backend.utils.word_manager import get_words


class Reset(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        # Send message to hide Reset button while resetting
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.RESET.value,
                'resetting': True
            }
        )

        private = await database_sync_to_async(is_private)(consumer.game_id)

        await database_sync_to_async(delete_game)(consumer.game_id)
        await database_sync_to_async(generate_game)(consumer.game_id, private)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.RESET.value,
                'resetting': False
            }
        )

    @staticmethod
    async def respond(consumer, event):
        if event['resetting']:
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RESET.value,
                'resetting': True
            }))
        elif consumer.white_team == -1:
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RESET.value,
                'resetting': False
            }))
        else:
            words = await database_sync_to_async(get_words)(consumer.game_id, consumer.white_team, True)
            game = await database_sync_to_async(get_game)(consumer.game_id)

            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RESET.value,
                'words': words,
                'game': game,
                'resetting': False
            }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
