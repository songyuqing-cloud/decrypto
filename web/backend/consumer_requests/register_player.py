import json

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.utils.enums import RequestTypeEnum
from backend.utils.game_manager import get_game
from backend.utils.player_manager import player_exists, add_player, get_player


class RegisterPlayer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        name = text_data_json['name']
        white_team = text_data_json['white_team']

        if await database_sync_to_async(player_exists)(consumer.game_id, name):
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.REGISTER_PLAYER.value,
                'error': 1
            }))
            return

        consumer.player_name = name
        consumer.white_team = white_team

        await database_sync_to_async(add_player)(consumer.game_id, consumer.player_name, consumer.white_team)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.REGISTER_PLAYER.value,
                'player_name': consumer.player_name,
            }
        )

    @staticmethod
    async def respond(consumer, event):
        player_name = event['player_name']

        game = await database_sync_to_async(get_game)(consumer.game_id, True)
        player = await database_sync_to_async(get_player)(consumer.game_id, player_name, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.REGISTER_PLAYER.value,
            'error': 0,
            'self': player_name == consumer.player_name,
            'player': player,
            'game': game
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'name' in text_data_json and \
               'white_team' in text_data_json and \
               0 < len(text_data_json['name']) <= 16
