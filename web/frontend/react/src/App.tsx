import React, { Component, ReactNode } from 'react'
import './App.css'

class App extends Component {
  render(): ReactNode {
    return <h1>Décrypto</h1>
  }
}

export default App
