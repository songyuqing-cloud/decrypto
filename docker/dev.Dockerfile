FROM node:latest AS frontend

WORKDIR /decrypto/frontend

ADD web/frontend/package.json /decrypto/frontend/package.json
ADD web/frontend/.babelrc /decrypto/frontend/.babelrc
ADD web/frontend/webpack.config.js /decrypto/frontend/webpack.config.js

RUN npm install

ADD web/frontend/ /decrypto/frontend/

RUN npm run dev


FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

RUN mkdir /decrypto

WORKDIR /decrypto

RUN apk add --update --no-cache --virtual .tmp alpine-sdk libffi-dev libressl-dev
RUN pip install --upgrade pip && pip install -r requirements.txt
RUN apk del .tmp

ADD web/requirements.txt /decrypto/requirements.txt

COPY --from=npm /decrypto/frontend/static/frontend/main.js /decrypto/frontend/static/frontend.main.js

ADD web/frontend/migrations /decrypto/frontend/migrations
ADD web/frontend/static /decrypto/frontend/static
ADD web/frontend/templates /decrypto/frontend/templates
ADD web/frontend/*.py /decrypto/frontend/

ADD web/backend /decrypto/backend
ADD web/decrypto /decrypto/decrypto
ADD web/*.py /decrypto/

WORKDIR /decrypto

ENV DJANGO_SETTINGS_MODULE=decrypto.settings_dev

ENV REDIS_HOST=redis
ENV REDIS_PORT=6379

CMD python manage.py migrate && python populate_database.py words 1 && python manage.py runserver 0.0.0.0:65000
